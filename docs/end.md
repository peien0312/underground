# 遊戲結束條件(End Game Conditions)

以下 4 個條件任一個滿足，該回合完畢結束遊戲：

1.  史詩級寶藏出土。
2.  史詩級怪獸殞落。
3.  第 1 層至第 4 層區域探索完畢。
4.  遊戲進行到第 16 個回合。

若有玩家在最後一回合死亡，則直接視為最後一名。若有多數玩家曾經死亡再比分數高低。
