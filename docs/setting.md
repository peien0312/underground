# 遊戲設置(Setting)

將遊戲版圖放置桌子中央，並將區域板塊分別依照深度洗勻面朝下放到遊戲版圖上。同顏色的區域為同一地層，總共有 5 層。每層包含 2 等深度。第 5 層較特殊，只包含 1 個深度。

![game board](board.jpg)

1.  將不同資源卡、道具卡和裝備卡依種類疊好，骰子備好，放在伸手可得之處。
2.  將怪獸卡依照深度分類，洗勻後面朝下放在對應的深度旁邊。
3.  各人選擇喜歡的顏色(不要打架！)，並取走對應玩家版圖、合成圖表。
4.  各人取走對應顏色的 5 個米寶，包含 1 個大米寶和 4 個小米寶。大米寶放在遊戲版圖上方的地面區域並呈站立狀態，小米寶放在玩家面前。其餘沒有使用的小米寶放在備用區。
5.  各人取走玩家指示物 5 個，將 3 個放在玩家圖板上血量[2]、精神[5]和黃金[0]的位置；1 個放在遊戲圖版邊緣分數[0]的位置；1 個放在玩家版圖上的墓碑。
6.  將史詩級寶藏分數指示物放在[9]的位置。
7.  將回合指示物放在[1]的位置。
8.  將起始玩家指示物交給最近吃土比較嚴重的玩家。
