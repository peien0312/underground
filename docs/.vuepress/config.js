module.exports = {
  title: 'Underground - 地底探險',
  description: '專案開發文件',
  themeConfig: {
    nav: [{
      text: '遊戲規則',
      link: '/'
    }],
    sidebar: ['background', 'setting', 'goal', 'action', 'death', 'trap', 'area', 'end', 'craft', 'item', 'monster', 'board'],
    lastUpdated: 'Last Updated', // string | boolean
  },
  base: '/underground/',
  dest: 'public'
}